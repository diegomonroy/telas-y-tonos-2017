<?php
/**
 * @package     Joomla.Site
 * @subpackage  com_users
 *
 * @copyright   Copyright (C) 2005 - 2016 Open Source Matters, Inc. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */

defined('_JEXEC') or die;

JHtml::_('behavior.keepalive');
JHtml::_('behavior.formvalidator');
?>
<!-- Begin Custom -->
	<div class="registration<?php echo $this->pageclass_sfx; ?>">
		<?php if ( $this->params->get('show_page_heading') ) : ?>
		<div class="page-header">
			<h1><?php echo $this->escape( $this->params->get('page_heading') ); ?></h1>
		</div>
		<?php endif; ?>
		<form id="member-registration" action="<?php echo JRoute::_('index.php?option=com_users&task=registration.register'); ?>" method="post" class="form-validate form-horizontal" enctype="multipart/form-data">
			<?php foreach ( $this->form->getFieldsets() as $fieldset ) : ?>
			<?php $fields = $this->form->getFieldset( $fieldset->name ); ?>
			<?php if ( count( $fields ) ) : ?>
			<?php foreach ( $fields as $field ) : ?>
			<?php if ( $field->hidden ) : ?>
			<?php echo $field->input; ?>
			<?php else : ?>
			<div class="row">
				<div class="col-xs-12 col-md-3">
					<?php echo $field->label; ?>
					<?php if ( ! $field->required && $field->type != 'Spacer' ) : ?>
					<span class="optional"><?php echo JText::_('COM_USERS_OPTIONAL'); ?></span>
					<?php endif; ?>
				</div>
				<div class="col-xs-12 col-md-9"><?php echo $field->input; ?></div>
			</div>
			<?php endif; ?>
			<?php endforeach; ?>
			<?php endif; ?>
			<?php endforeach; ?>
			<div class="text-center">
				<button type="submit" class="btn btn-default validate"><?php echo JText::_('JREGISTER'); ?></button>
				<a href="<?php echo JRoute::_(''); ?>" title="<?php echo JText::_('JCANCEL'); ?>" class="btn"><?php echo JText::_('JCANCEL'); ?></a>
			</div>
			<input type="hidden" name="option" value="com_users">
			<input type="hidden" name="task" value="registration.register">
			<?php echo JHtml::_('form.token'); ?>
		</form>
	</div>
<!-- End Custom -->