<?php

/* ************************************************************************************************************************

Telas & Tonos

File:			modules.php
Author:			Amapolazul Grupo Creativo
Homepage:		www.amapolazul.com
Copyright:		2016

************************************************************************************************************************ */

defined( '_JEXEC' ) or die( 'Acceso Restringido.' );

/*
 * Module chrome case 1
 */
function modChrome_case1( $module ) {
	echo $module->content;
}