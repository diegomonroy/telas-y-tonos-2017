<?php

/* ************************************************************************************************************************

Telas & Tonos

File:			index.php
Author:			Amapolazul Grupo Creativo
Homepage:		www.amapolazul.com
Copyright:		2016

************************************************************************************************************************ */

defined( '_JEXEC' ) or die( 'Acceso Restringido.' );

JHtml::_( 'behavior.framework', true );

// Variables

$site_base = $_SERVER['HTTP_HOST']; // e.g. www.amapolazul.com
$site_path = 'http://' . $site_base; // e.g. http://www.amapolazul.com
$app = JFactory::getApplication();
$option = JRequest::getVar( 'option' );
$view = JRequest::getVar( 'view' );
$Itemid = JRequest::getVar( 'Itemid' );
$pageclass = $app->getParams( 'com_content' );
$pageclass_sfx = $pageclass->get( 'pageclass_sfx' );
$user = JFactory::getUser();

// Template path

$path = 'templates/' . $this->template . '/';

// Modules

$show_banner = $this->countModules( 'banner' );
$show_block = $this->countModules( 'block' );
$show_bottom = $this->countModules( 'bottom' );
$show_collection = $this->countModules( 'collection' );
$show_left = $this->countModules( 'left' );
$show_menu = $this->countModules( 'menu' );
$show_navigation = $this->countModules( 'navigation' );
$show_navigation_bottom = $this->countModules( 'navigation_bottom' );
$show_top = $this->countModules( 'top' );

if ( $show_left ) {
	$style_banner = 'col-xs-12 col-sm-9';
	$style_component = 'col-xs-12 col-sm-9';
} else {
	$style_banner = 'col-lg-12';
	$style_component = 'col-lg-12';
}

// Params

?>
<!DOCTYPE html>
<html lang="<?php echo $this->language; ?>">
	<head>
		<jdoc:include type="head" />
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<meta name="google-site-verification" content="6zycSE_5avNBypmy9m4a_MgTDcy7HJL1xy-akH3TZcg">
		<!-- Begin Open Graph Protocol -->
		<meta property="og:url" content="<?php echo $site_path; ?>">
		<meta property="og:type" content="website">
		<meta property="og:title" content="<?php echo $app->getCfg( 'sitename' ); ?>">
		<meta property="og:description" content="<?php echo $app->getCfg( 'MetaDesc' ); ?>">
		<meta property="og:image" content="<?php echo $site_path; ?>/<?php echo $path; ?>images/logo_ogp.png">
		<link rel="image_src" href="<?php echo $site_path; ?>/<?php echo $path; ?>images/logo_link_ogp.png">
		<!-- End Open Graph Protocol -->
		<link href="<?php echo $path; ?>js/bootstrap/css/bootstrap.min.css" rel="stylesheet">
		<link href="<?php echo $path; ?>js/jquery/fancybox/jquery.fancybox.css?v=2.1.5" rel="stylesheet" type="text/css" media="screen">
		<link href="<?php echo $path; ?>js/jquery/owl-carousel/assets/owl.carousel.min.css" rel="stylesheet">
		<link href="<?php echo $path; ?>js/jquery/owl-carousel/assets/owl.theme.default.min.css" rel="stylesheet">
		<link href="<?php echo $path; ?>css/template.css" rel="stylesheet" type="text/css">
		<!--[if lt IE 9]>
		<script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
		<script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
		<![endif]-->
		<!-- Begin Google Analytics -->
		<script>

			(function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
				(i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
				m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
			})(window,document,'script','//www.google-analytics.com/analytics.js','ga');

			ga('create', 'UA-81723806-1', 'auto');
			ga('send', 'pageview');

		</script>
		<!-- End Google Analytics -->
	</head>
	<body>
		<?php if ( $show_top ) : ?>
		<!-- Begin Top -->
			<div class="top">
				<div class="container">
					<div class="row">
						<div class="col-lg-12">
							<jdoc:include type="modules" name="top" style="xhtml" />
						</div>
					</div>
				</div>
			</div>
		<!-- End Top -->
		<?php endif; ?>
		<?php if ( $show_menu ) : ?>
		<!-- Begin Menu -->
			<div class="menu_wrap">
				<div class="container">
					<div class="row">
						<jdoc:include type="modules" name="menu" style="xhtml" />
					</div>
				</div>
			</div>
		<!-- End Menu -->
		<?php endif; ?>
		<?php if ( $show_navigation ) : ?>
		<!-- Begin Navigation -->
			<div class="navigation">
				<div class="container">
					<div class="row">
						<jdoc:include type="modules" name="navigation" style="xhtml" />
					</div>
				</div>
			</div>
		<!-- End Navigation -->
		<?php endif; ?>
		<?php if ( $show_banner ) : ?>
		<!-- Begin Banner -->
			<div class="banner">
				<div class="container">
					<div class="row">
						<?php if ( $show_left ) : ?>
						<!-- Begin Left -->
							<div class="col-xs-12 col-sm-3">
								<jdoc:include type="modules" name="left" style="xhtml" />
							</div>
						<!-- End Left -->
						<?php endif; ?>
						<div class="<?php echo $style_banner; ?>">
							<jdoc:include type="modules" name="banner" style="xhtml" />
						</div>
					</div>
				</div>
			</div>
		<!-- End Banner -->
		<?php endif; ?>
		<?php if ( $show_collection ) : ?>
		<!-- Begin Collection -->
			<div class="collection">
				<div class="container">
					<div class="row">
						<div class="col-lg-12">
							<jdoc:include type="modules" name="collection" style="xhtml" />
						</div>
					</div>
				</div>
			</div>
		<!-- End Collection -->
		<?php else : ?>
		<?php if ( $option == 'com_content' && $view == 'featured' && $Itemid == 101 ) : else : ?>
		<!-- Begin Component -->
			<div class="component<?php echo $pageclass_sfx; ?>">
				<div class="container">
					<div class="row">
						<?php if ( $show_left ) : ?>
						<!-- Begin Left -->
							<div class="col-xs-12 col-sm-3">
								<jdoc:include type="modules" name="left" style="xhtml" />
							</div>
						<!-- End Left -->
						<?php endif; ?>
						<div class="<?php echo $style_component; ?>">
							<jdoc:include type="component" />
						</div>
					</div>
				</div>
			</div>
		<!-- End Component -->
		<?php endif; ?>
		<?php endif; ?>
		<?php if ( $show_block ) : ?>
		<!-- Begin Block -->
			<div class="block">
				<div class="container">
					<div class="row">
						<div class="col-lg-12">
							<jdoc:include type="modules" name="block" style="xhtml" />
						</div>
					</div>
				</div>
			</div>
		<!-- End Block -->
		<?php endif; ?>
		<?php if ( $show_navigation_bottom ) : ?>
		<!-- Begin Navigation Bottom -->
			<div class="navigation">
				<div class="container">
					<div class="row">
						<jdoc:include type="modules" name="navigation_bottom" style="xhtml" />
					</div>
				</div>
			</div>
		<!-- End Navigation Bottom -->
		<?php endif; ?>
		<?php if ( $show_bottom ) : ?>
		<!-- Begin Bottom -->
			<div class="bottom">
				<div class="container">
					<div class="row">
						<jdoc:include type="modules" name="bottom" style="xhtml" />
					</div>
				</div>
			</div>
		<!-- End Bottom -->
		<?php endif; ?>
		<!-- Begin Copyright -->
			<div class="copyright_wrap">
				<div class="copyright">
					&copy; <?php echo date( 'Y' ); ?> <a href="<?php echo $this->baseurl; ?>"><?php echo $app->getCfg( 'sitename' ); ?></a>. Todos los derechos reservados. Sitio desarrollado por <a href="http://www.amapolazul.com" target="_blank">Amapola Azul</a>.
				</div>
			</div>
		<!-- End Copyright -->
		<!-- Begin Main Scripts -->
			<script src="<?php echo $path; ?>js/jquery/jquery.js"></script>
			<script src="<?php echo $path; ?>js/bootstrap/js/bootstrap.min.js"></script>
			<script src="<?php echo $path; ?>js/jquery/fancybox/jquery.fancybox.pack.js?v=2.1.5" type="text/javascript"></script>
			<script src="<?php echo $path; ?>js/jquery/owl-carousel/owl.carousel.min.js" type="text/javascript"></script>
			<script src="<?php echo $path; ?>js/jquery/scripts.js" type="text/javascript"></script>
		<!-- End Main Scripts -->
	</body>
</html>