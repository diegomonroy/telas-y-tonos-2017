// JavaScript Document

/* ************************************************************************************************************************

Telas & Tonos

File:			scripts.js
Author:			Amapolazul Grupo Creativo
Homepage:		www.amapolazul.com
Copyright:		2016

************************************************************************************************************************ */

jQuery.noConflict();

jQuery(document).ready(function() {
	// Owl Carousel
	jQuery( '.owl-carousel' ).owlCarousel({
		center: true,
		navText: ['Anterior', 'Siguiente'],
		responsiveClass: true,
		responsive: {
			0: {
				items: 1,
				nav: true
			}
		}
	});
	// fancyBox
	jQuery( '.fancybox' ).fancybox();
});